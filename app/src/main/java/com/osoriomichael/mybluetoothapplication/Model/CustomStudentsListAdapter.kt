package com.osoriomichael.mybluetoothapplication.Model

import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.osoriomichael.mybluetoothapplication.R

class CustomStudentsListAdapter(private val studentsList: List<Student>): RecyclerView.Adapter<CustomStudentsListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.student_layout,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = studentsList.size


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val student = studentsList[p1]
        p0.txtName.text = String.format(p0.itemView.resources.getString(R.string.student_name),student.name)
        p0.txtCode.text = String.format(p0.itemView.resources.getString(R.string.student_code),student.code)
        p0.txtMac.text = String.format(p0.itemView.resources.getString(R.string.student_mac),student.mac)
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        val txtName = itemView.findViewById(R.id.txtName) as TextView
        val txtCode = itemView.findViewById(R.id.txtCode) as TextView
        val txtMac= itemView.findViewById(R.id.txtMac) as TextView
    }
}