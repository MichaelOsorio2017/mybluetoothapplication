package com.osoriomichael.mybluetoothapplication.Model

data class Student(var code: Int, var name: String, var mac: String)