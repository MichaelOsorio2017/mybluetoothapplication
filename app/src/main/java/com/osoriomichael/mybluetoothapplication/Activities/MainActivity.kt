package com.osoriomichael.mybluetoothapplication.Activities

import android.bluetooth.BluetoothAdapter
import android.content.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.osoriomichael.mybluetoothapplication.Extensions.toast
import com.osoriomichael.mybluetoothapplication.R
import kotlinx.android.synthetic.main.activity_main.*
import java.time.LocalDateTime

class MainActivity : AppCompatActivity() {
    companion object{
        const val REQUEST_ENABLE_BT = 0
        const val REQUEST_DISCOVER_BT = 1
        var stopTimer = false
        private val currentDate = LocalDateTime.now().toString().split("T")[0].split("-")
        val DAY = currentDate[2]
        val MONTH = currentDate[1].toInt() -1
        val YEAR = currentDate[0]
    }

    private var clipboardManager:ClipboardManager? = null
    private val url = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$DAY-$MONTH-$YEAR/students/201616273"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnActivateDiscovery = btnActivateDiscovery
        val btnCopyMac = btnCopyMac
        val imgCheck = imgCheck
        val imgUnCheck = imgUnCheck
        val txAttendanceStatus = txAttendanceStatus
        val btnStopDiscovering = btnStopDiscovery
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val btnCheckAbsentList = btnCheckAbsentList
        clipboardManager = (getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)
        resetInitialStatus()
        btnCopyMac.setOnClickListener {
            ClipData.newPlainText("text",txMacDirection.text.split(" ")[2].trim()).run{
                clipboardManager?.primaryClip = this
            }
            toast("Text Copied: ${clipboardManager?.primaryClip?.getItemAt(0)?.text?.toString()}")
        }

        btnActivateDiscovery.setOnClickListener {

            bluetoothAdapter?.run {
                    if(!isEnabled){
                        this.enable()
                        Thread.sleep(100)
                    }
                    startDiscovery()
                    Thread.sleep(100)
                    if(isDiscovering){
                        txBluetoothStatus.text = getString(R.string.bluetooth_discovering_status)
                        object: CountDownTimer(80000,1000){
                            override fun onTick(millisUntilFinished: Long) {
                                checkAttendanceList()
                                val quantity = (millisUntilFinished/1000).toInt()
                                txTimeRemaining.text = resources.getQuantityString(R.plurals.num_sec_to_stop,quantity,quantity)
                                if((millisUntilFinished-1000)/1000%12 == 0L){
                                    cancelDiscovery()
                                    startDiscovery()
                                }
                                if(stopTimer){
                                    cancelDiscovery()
                                    resetInitialStatus(false)
                                    cancel()
                                }
                            }
                            override fun onFinish() {
                                cancelDiscovery()
                                resetInitialStatus(false)
                            }
                        }.start()
                    }
                }?:toast("Device does not support Bluetooth")
        }
        btnStopDiscovering.setOnClickListener {
            stopTimer = true
        }

        btnCheckAbsentList.setOnClickListener {
            startActivity(Intent(this, AbsentListActivity::class.java))
        }
    }

    private fun resetInitialStatus(changeImage:Boolean = true){
        txBluetoothStatus.text = getString(R.string.bluetooth_sleep_status)
        txTimeRemaining.text = getString(R.string.default_time)
        if(imgCheck.visibility == View.VISIBLE && changeImage){
            imgCheck.visibility = View.GONE
            imgUnCheck.visibility = View.VISIBLE
        }
        stopTimer = false
    }
    private fun checkAttendanceList(){
        Volley.newRequestQueue(this).run{
            add(StringRequest(Request.Method.GET,url,
                    Response.Listener<String> { response ->
                        if(!response.contains("404")){
                            imgUnCheck.visibility = View.GONE
                            imgCheck.visibility = View.VISIBLE
                            txAttendanceStatus.text = getString(R.string.signed_attendance)
                            stopTimer = true
                        }
                    },
                    Response.ErrorListener { }
                )
            )
        }
    }
}
