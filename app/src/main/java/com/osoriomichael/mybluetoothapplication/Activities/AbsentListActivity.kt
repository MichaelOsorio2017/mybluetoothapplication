package com.osoriomichael.mybluetoothapplication.Activities

import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.osoriomichael.mybluetoothapplication.Extensions.toast
import com.osoriomichael.mybluetoothapplication.Model.CustomStudentsListAdapter
import com.osoriomichael.mybluetoothapplication.Model.Student
import com.osoriomichael.mybluetoothapplication.R
import kotlinx.android.synthetic.main.activity_absent_list.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*

class AbsentListActivity : AppCompatActivity() {

    private val studentsListUrl = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList/"
    private var currentStudentsListUrl = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/${MainActivity.DAY}-${MainActivity.MONTH}-${MainActivity.YEAR}/students"

    companion object{
        const val studentsListFileName = "studentsList"
        private var absentList: List<Student> = mutableListOf()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absent_list)
        val btnRefreshStudentsList = btnRefreshStudentsList
        val btnCheckAbsentList = btnCheckAbsentList
        val recyclerView = rcvAbsentList
        val btnSelectDate = btnSelectDate
        val txtDate = txtDate
        val btnSaveAbsentList = btnPersistData
        txtDate.text = String.format(resources.getString(R.string.date),MainActivity.DAY,MainActivity.MONTH,MainActivity.YEAR)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL,false)

        btnSelectDate.setOnClickListener {
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)
            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                currentStudentsListUrl = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$dayOfMonth-$monthOfYear-$year/students"
                txtDate.text = String.format(resources.getString(R.string.date),dayOfMonth.toString(),monthOfYear.toString(),year.toString())
            }, y, m, d)

            datePickerDialog.show()
        }

        btnRefreshStudentsList.setOnClickListener {
            Volley.newRequestQueue(this).run {
                add(StringRequest(
                        Request.Method.GET,studentsListUrl,
                        Response.Listener<String> { response ->
                            val fileInputStream = openFileOutput(studentsListFileName, Context.MODE_PRIVATE)
                            fileInputStream.write(response.toByteArray())
                            toast("Student list update")
                            checkAbsentList()
                        },
                        Response.ErrorListener { toast("There is an error retrieving the students list")}
                    )
                )
            }
        }
        btnCheckAbsentList.setOnClickListener {
            checkAbsentList()
        }
        btnSaveAbsentList.setOnClickListener {
            if(Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED){
                val file = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS),"absentList_${txtDate.text}")
                if(!file?.mkdirs()){
                    print("Directory no created")
                }else{
                    file.bufferedWriter().write(absentList.toString())
                }
            }
        }
    }

    private fun checkAbsentList(){
        Volley.newRequestQueue(this).run{
            add(
                StringRequest(Request.Method.GET,currentStudentsListUrl,
                    Response.Listener<String> { response ->
                        val fileInputStream = openFileInput(studentsListFileName)
                        val stringBuilder = StringBuilder()

                        BufferedReader(InputStreamReader(fileInputStream)).forEachLine {
                                stringBuilder.append(it)
                        }
                        val responseJSON = JSONObject(response)
                        val studentsList = JSONObject(stringBuilder.toString()).getJSONArray("documents")
                        val studentsDataList = mutableListOf<Student>()
                        for(i in 0 until studentsList.length()){
                            (studentsList[i] as JSONObject).getJSONObject("fields").let { fields ->
                                val name = fields.getJSONObject("name").getString("stringValue").trim()
                                val mac = fields.getJSONObject("mac").getString("stringValue").trim()
                                val code = fields.getJSONObject("code").getString("stringValue").trim()
                                try{
                                    studentsDataList.add(Student(code.toInt(),name,mac))
                                }catch (e: Exception){
                                    e.printStackTrace()
                                }
                            }
                        }
                        if(responseJSON.length() != 0){
                            val currentStudentList = responseJSON.getJSONArray("documents")
                            val currentStudentsDataList = mutableListOf<Student>()

                            for(i in 0 until currentStudentList.length()){
                                (currentStudentList[i] as JSONObject).getJSONObject("fields").let { fields ->
                                    val name = fields.getJSONObject("name").getString("stringValue").trim()
                                    val mac = fields.getJSONObject("mac").getString("stringValue").trim()
                                    val code = fields.getJSONObject("code").getString("stringValue").trim()
                                    try{
                                        currentStudentsDataList.add(Student(code.toInt(),name,mac))
                                    }catch (e: Exception){
                                        e.printStackTrace()
                                    }
                                }
                            }
                            print("ESTUDENT DATA LIST: " + studentsDataList)
                            print("CURRENT STUDENT DATA LIST: " + currentStudentList)
                            absentList = studentsDataList.filter { !currentStudentsDataList.contains(it) }
                        }else{
                            absentList = studentsDataList
                            toast("Nobody have been signed the attendance list")
                        }
                        val adapter = CustomStudentsListAdapter(absentList)
                        rcvAbsentList.adapter = adapter
                    },
                    Response.ErrorListener { toast("There is a problem retrieving the current students list in room")}
                )
            )
        }
    }
}
